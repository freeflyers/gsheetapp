package com.deltacoder.gsheetapp.repository

import android.util.Log
import com.deltacoder.gsheetapp.NetWorkApi
import com.deltacoder.gsheetapp.model.DataWrapperList
import retrofit2.Call
import retrofit2.Response


class DataRepository(val netWorkApi: NetWorkApi) {

    fun getData(onData: OnDataList) {
        netWorkApi.getGList().enqueue(object : retrofit2.Callback<DataWrapperList> {
            override fun onResponse(call: Call<DataWrapperList>, response: Response<DataWrapperList>) {
                onData.onSuccess((response.body() as DataWrapperList))
            }

            override fun onFailure(call: Call<DataWrapperList>, t: Throwable) {
                Log.d("Exe ###: ",t.message.toString())
                onData.onFailure()
            }
        })
    }

    interface OnDataList {
        fun onSuccess(data: DataWrapperList)
        fun onFailure()
    }
}

