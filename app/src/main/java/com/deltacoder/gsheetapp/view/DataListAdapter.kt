package com.deltacoder.gsheetapp.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.deltacoder.gsheetapp.R


class DataListAdapter(private val dataList: List<List<String>>) :
    RecyclerView.Adapter<DataListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.adapter_data_list_item, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.city?.text = dataList[position].get(0)
        viewHolder.country?.text = dataList[position].get(1)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val city = itemView.findViewById<TextView>(R.id.tv_city)
        val country = itemView.findViewById<TextView>(R.id.tv_country)
    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}