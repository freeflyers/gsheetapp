package com.deltacoder.gsheetapp.view

import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.deltacoder.gsheetapp.viewmodel.DataViewModel
import com.deltacoder.gsheetapp.R
import com.deltacoder.gsheetapp.model.DataWrapperList
import org.koin.android.viewmodel.ext.android.viewModel

class DataListFragment : Fragment() {

    private val dataListModel: DataViewModel by viewModel()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_data_list, container, false)
    }


    override fun onStart() {
        super.onStart()

        val recyclerView = view?.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView!!.layoutManager =
            LinearLayoutManager(requireView().context, LinearLayoutManager.VERTICAL, false)


        dataListModel.getData()
        dataListModel.listOfData.observe(this, Observer(function = fun(data: DataWrapperList?) {
            data?.let {
                val dataList: List<List<String>> = data.values
                val dataListAdapter = DataListAdapter(dataList)
                recyclerView.adapter = dataListAdapter

            }
        }))
    }


}
