package com.deltacoder.gsheetapp.model

import com.google.gson.annotations.SerializedName

data class DataWrapperList(
    @SerializedName("range")
    var range: String,
    @SerializedName("majorDimension")
    var majorDimension: String,
    @SerializedName("values")
    var values: List<List<String>>
)