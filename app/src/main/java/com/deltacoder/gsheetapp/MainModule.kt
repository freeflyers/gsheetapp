package com.deltacoder.gsheetapp

import com.deltacoder.gsheetapp.repository.DataRepository
import com.deltacoder.gsheetapp.viewmodel.DataViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val mainModule = module {

    single { DataRepository(get()) }

    single { createWebService() }

    viewModel { DataViewModel(get()) }

}

fun createWebService(): NetWorkApi {
    val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl("https://sheets.googleapis.com/v4/spreadsheets/<SHEET ID>/values/")
        .build()

    return retrofit.create(NetWorkApi::class.java)
}