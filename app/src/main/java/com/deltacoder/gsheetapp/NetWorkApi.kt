package com.deltacoder.gsheetapp


import com.deltacoder.gsheetapp.model.DataWrapperList
import retrofit2.Call
import retrofit2.http.GET

interface NetWorkApi{

    @GET("Sheet1?key=<YOUR KEY>")
    fun getGList(): Call<DataWrapperList>

}