package com.deltacoder.gsheetapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.deltacoder.gsheetapp.model.DataWrapperList
import com.deltacoder.gsheetapp.repository.DataRepository
import org.koin.standalone.KoinComponent


class DataViewModel(val dataRepository: DataRepository) : ViewModel(), KoinComponent {

    var listOfData = MutableLiveData<DataWrapperList>()

    init {
        listOfData.value
    }

    fun getData() {
        dataRepository.getData(object : DataRepository.OnDataList {
            override fun onSuccess(data: DataWrapperList) {
                listOfData.value = data
            }

            override fun onFailure() {
                //REQUEST FAILED
            }
        })
    }
}